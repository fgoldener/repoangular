import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Mensaje } from '../Model/Mensaje';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  mensaje:Mensaje[];
  

  constructor(private http:HttpClient) {
    
   }
   Url='http://192.168.99.100:32505/test1App/api/v1/mensajes'

  

  getMensajes(){
    return this.http.get<Mensaje[]>(this.Url);
  }

  createMensaje(mensaje:Mensaje){
    return this.http.post<Mensaje>(this.Url,mensaje);
  }

  getMensajeId(id:number){
    return this.http.get<Mensaje>(this.Url+"/"+id);
  }

  updateMensaje(mensaje:Mensaje){
    return this.http.put<Mensaje>(this.Url+"/"+mensaje.id,mensaje);
  }
  deleteMensaje(mensaje:Mensaje){
    return this.http.delete<Mensaje>(this.Url+"/"+mensaje.id);
  }
}
