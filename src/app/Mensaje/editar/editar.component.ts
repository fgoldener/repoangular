import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';
import { Router } from '@angular/router';
import { Mensaje } from 'src/app/Model/Mensaje';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {
  mensaje:Mensaje=new Mensaje();

  constructor(private router:Router,private service:ServiceService) { }

  ngOnInit() {
    this.Editar();
  }

  Editar(){
    let id=localStorage.getItem("id");
    this.service.getMensajeId(+id)
    .subscribe(data=>{
      this.mensaje=data;
    })
  }
  Actualizar(mensaje:Mensaje){
    this.service.updateMensaje(mensaje)
    .subscribe(data=>{
      alert("Actualizacion Exitosa");
      this.router.navigate(["listar"]);
    })
  }

}
