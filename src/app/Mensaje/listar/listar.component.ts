import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ServiceService}from '../../Service/service.service'
import { Mensaje } from 'src/app/Model/Mensaje';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

  mensajes:Mensaje[];

  constructor(private service:ServiceService, private router:Router) { }

  ngOnInit() {
    this.service.getMensajes()
    .subscribe(data=>{
      this.mensajes=data;
    })

  }

  Editar(mensaje:Mensaje):void{
    localStorage.setItem("id",mensaje.id.toString());
    this.router.navigate(["editar"]);

  }

  Eliminar(mensaje:Mensaje){
    this.service.deleteMensaje(mensaje)
    .subscribe(data=>{
      this.mensajes=this.mensajes.filter(m=>m!==mensaje);
      alert("Mensaje Eliminado");
    })
  }

}
