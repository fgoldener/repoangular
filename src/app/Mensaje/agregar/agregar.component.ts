import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/Service/service.service';
import { Mensaje } from 'src/app/Model/Mensaje';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {
  mensaje:Mensaje=new Mensaje();

  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit() {
  }
  Agregar(){
    this.service.createMensaje(this.mensaje)
    .subscribe(data=>{
      alert("Ingreso Exitoso");
      this.router.navigate(["listar"]);
    })
  } 
    
  


}
